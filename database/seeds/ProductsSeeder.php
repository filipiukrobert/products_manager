<?php

use Illuminate\Database\Seeder;

use App\Models\Price;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Schema::disableForeignKeyConstraints();

    	$items_count = 15;

        $products = factory(App\Models\Product::class, $items_count)->create();

        foreach ($products as $product) {

        	$prices_count = rand(1,5);

        	dump("Product id: ", $product->id);

        	for ($i=0; $i < $prices_count; $i++) { 

        		$value = 1000 / rand(1,100);
        		
        		$price = new Price;
        		$price->product_id = $product->id;
        		$price->value = $value;
        		dump("Price nr" . ($i+1), $price);
        		$price->save();
        	}
        }

        // Schema::enableForeignKeyConstraints();
    }
}
