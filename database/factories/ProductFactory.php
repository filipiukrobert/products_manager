<?php

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {

	$maxChars = 10;

    return [
        "name" => $faker->realText($maxChars = 30),
        "description" => $faker->realText,
    ];
});
