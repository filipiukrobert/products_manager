let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// JQuery
mix.copy("node_modules/jquery/dist/jquery.min.js", "public/js/jquery.min.js");

// Semantic UI
mix.copy("semantic/dist/semantic.min.js", "public/js/semantic.min.js");
mix.copy("semantic/dist/semantic.min.css", "public/css/semantic.min.css");
mix.copy("semantic/dist/themes", "public/css/themes");

//  General

/// CSS
mix.styles([
	"resources/assets/css/master.css",
	"resources/assets/css/_form.css",
	], "public/css/general.css");
mix.copy("resources/assets/css/rwd.css","public/css/rwd.css")

// mix.copy("resources/assets/css/master.css","public/css/master.css",)
// mix.copy("resources/assets/css/_form.css","public/css/_form.css",)

/// JS
mix.copy("resources/assets/js/_form.js", "public/js/_form.js");
mix.copy("resources/assets/js/deleteProduct.js", "public/js/deleteProduct.js");
mix.copy("resources/assets/js/productFormValidation.js", "public/js/productFormValidation.js");
mix.copy("resources/assets/js/FormManager.js", "public/js/FormManager.js");
mix.copy("resources/assets/js/index.js", "public/js/index.js");