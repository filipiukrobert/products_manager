# Used technologies

+ Laravel 		5.5.29
+ JQuery 		3.2.1
+ Semantic UI 	2.2


# Installation
## Warning! Make sure that You're using [Composer](https://getcomposer.org/)!

## Pulling repository example:
Go to root folder of your server and type:

`git clone https://filipiukrobert@bitbucket.org/filipiukrobert/netoholics.git DIRECTORY_NAME`

In case of using this example:
`DIRECTORY_NAME`, further in this readme, will be named `root_folder`
## ---------------------------------



After pulling repo, open terminal, go to `root_folder/` (e.g. folder named `www`) and then type:

`composer install`

`npm install` (this will probably take a while)

#### IMPORTANT! When installing Semantic UI via `npm install` make sure to use `automatic` installation and install Semantic UI in `root folder`

After finishing installing all packages via `npm install`

go to `root_folder/semantic`
and type: `gulp build`

After finishing build go back to `root_folder/`.

Type `cp .env.example .env`.

Open file `.env`.

Change values of fields (Type in Your database connection informations):
DB_DATABASE=`YOUR_DATABASE_NAME`
DB_USERNAME=`DATABASE_USER` (mostly by default: "root")
DB_PASSWORD=`DATABASE_PASSWORD` (mostly by default: "")

Open terminal, go to `root_folder/` and type:

`php artisan key:generate`
`php artisan migrate`

And finally:

`npm run dev`

## Installation done!

If you have problems, restarting your server might help.

# Seeding database

If you want to seed database **(ONLY After COMPLETE installation)**
in terminal, go to `root_folder/` and type:

`php artisan db:seed`

This command will run ProductsSeeder which will seed your database with  dummy data.
