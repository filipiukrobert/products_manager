
$(window).ready(function() {

	let sort_class = getURLParameterValue("sort_type") + "ending";
	let sort_by = getURLParameterValue("sort_by");

	$("th span[data-sort='"+sort_by+"'] i.sort").removeClass().addClass(sort_class + " sort icon");

	if( sort_by == "prices" ) {
		let search_query_input = $(".ui.search.input input[name='search_query']");
		let pattern = "(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)";

		$(search_query_input).attr("pattern", pattern);
	}

	let actionInputText = $(".ui.action.input .text").text();
	$(".ui.action.input .text").text(actionInputText.trim());

	$("#loader_dimmer").removeClass("active");
});

function getURLParameterValue(parameter) {
	var pageURL = window.location.search.substring(1);
	var variables = pageURL.split('&');

	for (var i = 0; i < variables.length; i++) {
		var parameterName = variables[i].split('=');

		if (parameterName[0] == parameter) {
			return parameterName[1];
		}
	}
}

$("td:not([data-type='actions'])").on("click", function() {
	
	let tr = $(this).parent("tr");
	let trContent = $(tr).find("td");
	console.log(tr, trContent);

	populatePreviewModal(tr);

	$(".ui.product_preview.modal").modal("show");
});

$("td[data-type='prices']").each(function(index, element) {

	if( hasMoreThanOneChild($(element).children(".ui.list")) ) {
		$(element).children(".ui.list").css("overflow-y","visible");
		$(element).children(".no_list").hide();
	} else {
		$(element).children(".ui.list").hide();
	}

});

function hasMoreThanOneChild(element) {
	if($(element).children().length > 1) {
		return true;
	} else {
		return false;
	}
}

function isOverflown(element) {
	return element.prop("scrollHeight") > element.prop("clientHeight");
}

function populatePreviewModal(tr) {

	let href = $(tr).attr("data-edit_href");
	let name = $(tr).find("td[data-type='name']").text();
	let description = $(tr).find("td[data-type='description']").text();
	let prices = $(tr).find("td[data-type='prices']").html();

	let modal = $(".ui.product_preview.modal");
	let modalContent = $(".ui.product_preview.modal .content");

	$(modal).find("a.ui.ok.button").attr("href", href);
	$(modalContent).find(".product_name").text(name);
	$(modalContent).find(".product_description").text(description);
	$(modalContent).find(".product_prices").html(prices);

	console.log(modalContent);
}

$(".ui.search.input .ui.dropdown .menu .item").on("click", function() {
	let item = $(this);
	let search_by = $(this).attr("data-type");
	let search_query_input = $(".ui.search.input input[name='search_query']");

	let pattern = "(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)";

	if( item.text() == "Prices" ) {
		$(search_query_input).attr("placeholder", "0.00");
	} else {
		$(search_query_input).attr("placeholder", "Search");
	}

	unsetHighlightSearchInput($(item));

	$(".ui.search.input .text").attr("data-search_by", search_by);
});

$("th:not(#actions)").on("click", function() {

	let sort_by = $(this).children("span.sort").attr("data-sort");
	let sort_icon = $(this).children("span.sort").children("i.sort");

	let sort_type;

	if( $(sort_icon).hasClass("ascending") ) {
		sort_type = "desc";
	} else if( $(sort_icon).hasClass("descending") ) {
		sort_type = "";
	} else {
		sort_type = "asc";
	}

	$(".ui.filters.form input[name='sort_by']").val(sort_by);
	$(".ui.filters.form input[name='sort_type']").val(sort_type);

	$(".ui.filters.form").trigger("submit");
});

$(".pagination .menu .item").on("click", function() {

	let value = $(this).text();
	let input = $(this).parents().find("input[name='per_page']");

	$(input).val(value);

	$(".ui.filters.form").trigger("submit");
});

$(".ui.filters.form").on("submit", function() {

	let searchInput = $(".ui.search.input input[name='search_query']");
	let search_by = $(".ui.search.input .text").attr("data-search_by");

	let pattern = "(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)";

	if( search_by == "Prices" && (!validateSearchInput(searchInput, pattern)) ) {
		
		setHighlightSearchInput(searchInput);
		return false;
	} else {
		unsetHighlightSearchInput(searchInput);
		$(".ui.search.input input[name='search_by']").val(search_by);
	}

	// return false;
});

$(".ui.action.input input[name='search_query']").on("focus", function() {
	unsetHighlightSearchInput($(this));
});

function validateSearchInput(searchInput, pattern) {

	let inputValue = $(searchInput).val();

	if( inputValue == "" ) {
		return true;
	}

	let regExp = new RegExp("(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)", "g");

	return regExp.test(inputValue);
}

function setHighlightSearchInput(searchInput) {
	$(searchInput).parents(".field").addClass("error");
};

function unsetHighlightSearchInput(searchInput) {
	$(searchInput).parents(".field").removeClass("error");
};