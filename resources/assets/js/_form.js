
var formManager = new FormManager();

$(window).ready(function() {
	formManager.loaderHide();
})

$("input,textarea").on("keyup", function() {
	formManager.unsetHighlightFieldError($(this).attr("name"));
});

$("input.price").on("keyup", function() {
	formManager.unsetHighlightFieldError("prices");
});

$("#productForm").on("submit", function(event) {

	formManager.loaderShow();

	formManager.clearErrors();
	formManager.hideErrors();

	formManager.fillPricesInput();

	formManager.sendForm();

	return false;
});

$(".ui.button.add_price").on("click", function() {

	let priceInput = $("input.price");
	let priceValue = $(priceInput).val();

	let regExp = new RegExp($(priceInput).attr("pattern"), "g");

	console.log("regExp", regExp);

	if(priceValue !== "" && regExp.test(priceValue)){
		
		formManager.addNewPrice(priceValue);

	} else {

		formManager.setHighlightFieldError("prices");
	}
});

$(window).ready(function() {
	formManager.togglePricesList();
});

$(document).on("click", ".ui.list .item", function() {
	formManager.removePrice($(this));

	formManager.togglePricesList();
});

// function isInputEmpty(name) {
// 	let pricesInputValue = $("input[name='"+name+"']").val();

// 	console.log(pricesInputValue);

// 	if(!pricesInputValue) {
// 		return false;
// 	} else {
// 		return true;
// 	}
// }