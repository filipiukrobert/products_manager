function FormManager() {

	this.sendForm = () => {
		sendFormPromise({
			headers: {
				"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
			},
			url: $("#productForm").attr("action"),
			method: "POST",
			data: $("#productForm").serialize(),
		})
		.then((data) => {
			console.log("sendFormPromise: success",data);

			if( data.status === "failure") {
				this.loaderHide();

				console.log(data.errors);
				this.populateErrorsList(data.errors);

			} else {

				this.redirect(window.location.origin);
			}

		})
		.catch((error) => {
			this.loaderHide();
			console.log("sendFormPromise: error" + error);
		});
	};

	this.redirect = (url) => {
		window.location.href = url;
	}

	//fillErrorsList
	this.populateErrorsList = (errors) => {
		$(errors).each((key, value) => {
			$(".ui.form .ui.error.message .ui.list").append("<li>"+value.text+"</li>");
			this.setHighlightFieldError(value.parameter);
		});

		this.showErrors();
	};

	this.fillPricesInput = () => {
		let pricesInput = $("input[name='prices']");

		let pricesList = $(".prices .ui.list");
		let pricesItems = pricesList.find(".item");

		var prices = [];

		$(pricesItems).each(function(index, element) {
			prices.push($(element).attr("data-price"));
		});

		console.log(prices);

		if(prices.length != 0) {
			var pricesJSON = JSON.stringify(prices);
			$(pricesInput).val(pricesJSON);

			console.log(pricesJSON);
		} else {
			$(pricesInput).val("");
		}

	};

	this.addNewPrice = (price) => {
		let formatedPrice = this.formatPrice(price);

		this.hydrateItemReference(formatedPrice);
		this.copyItemReference(".prices .ui.list");
		this.resetItemReference();
		this.clearPriceInput();

		this.togglePricesList();
	};

	this.formatPrice = (price) => {

		let formatedPrice = price;

		if( price.indexOf(".") == -1 ) {
			formatedPrice += ".00";
		} else {

			let priceSplit = formatedPrice.split(".");

			let integer_part = priceSplit[0];
			let fractional_part = priceSplit[1];

			if( fractional_part.length == 0 ) {
				formatedPrice += "00";
			} else if (fractional_part.length == 1) {
				formatedPrice += "0";
			}

		}

		return formatedPrice;
	};

	this.hydrateItemReference = (price) => {
		$(".item.reference").attr("data-price",price);
		$(".item.reference .content").text(price);
	};

	this.copyItemReference = (element) => {
		$(".item.reference").clone().appendTo(element);
		$(element+" .reference").css("display", "").removeClass("reference");
	};

	this.resetItemReference = () => {
		$(".item.reference").attr("data-price","");
		$(".item.reference .content").text("");
	};

	this.clearPriceInput = () => {
		$("input.price").val("");
	};

	this.togglePricesList = () => {

		let pricesList = $(".prices .ui.list");
		let prices = pricesList.find(".item");

		if(prices.length == 0) {
			$(".no_items_message").css("display","");
			$(".prices").css("display","none");
		} else {
			$(".no_items_message").css("display","none");
			$(".prices").css("display","");
		}

	};

	this.setHighlightFieldError = (name) => {
		$(".field[data-name='"+name+"'").addClass("error");
	};

	this.unsetHighlightFieldError = (name) => {
		$(".field[data-name='"+name+"'").removeClass("error");
	};

	this.loaderShow = () => {
		$("#loader_dimmer").addClass("active");
	};

	this.loaderHide = () => {
		$("#loader_dimmer").removeClass("active");
	};

	this.clearErrors = () => {
		$(".ui.form .ui.error.message .ui.list").html("");
	};

	this.showErrors = () => {
		$(".ui.form .ui.error.message").show();
	};

	this.hideErrors = () => {
		$(".ui.form .ui.error.message").hide();
	};

	this.removePrice = (item) => {
		$(item).remove();
	};

}



function sendFormPromise(options){
	return new Promise(function(resolve, reject) {
		$.ajax(options).done(resolve).fail(reject);
	});
}