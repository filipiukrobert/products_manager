$(document).on("click", "#delete_product", function() {
	$(".ui.basic.delete.modal").modal("show");
});

$(".ui.basic.delete.modal").modal({
	onApprove: function() {
		console.log("Delete product approve");
		$("#deleteForm").trigger("submit");
	}
});