@extends("master")

@section("menu")

@component("templates/_menu")
	@slot("home")
	active
	@endslot
	@slot("add_product")
	@endslot

@endcomponent

@endsection

@section("content")

<div class="ui index black segment">
	<a class="ui labelled icon blue add_product button" href="{{ route("product.new.get") }}">
		<i class="plus icon"></i>
		{{TranslatorHelper::translate("Add product")}}
	</a>

	<div class="ui filters teal segment">

		<form class="ui filters form" method="get" action="{{ route("index.get") }}" >

			<input type="hidden" name="sort_by" 
			@if(isset($sort_by))
			value="{{ $sort_by }}"
			@endif
			>
			<input type="hidden" name="sort_type" 
			@if(isset($sort_type))
			value="{{ $sort_type }}"
			@endif
			>

			<div class="two fields">
				<div class="field">
					<div class="ui selection pagination dropdown">
						@if(isset($params["per_page"]))
						<input type="hidden" name="per_page" value="{{ $params['per_page'] }}">
						<i class="dropdown icon"></i>

						<div class="text">{{ $params["per_page"] }}</div>
						@else
						<input type="hidden" name="per_page">
						<i class="dropdown icon"></i>
						<div class="default text">{{TranslatorHelper::translate("Pagination")}}</div>
						@endif
						<div class="menu">
							<div class="item" data-value="5">5</div>
							<div class="item" data-value="10">10</div>
							<div class="item" data-value="20">20</div>
							<div class="item" data-value="30">30</div>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="ui action right labeled search input">

						<input type="text" 
						@if(isset($params["search_by"]) && $params["search_by"] == "Prices"))
						placeholder="0.00"
						@else
						placeholder="{{TranslatorHelper::translate("Search")}}"
						@endif 
						name="search_query"
						@if(isset($params["search_query"]))
						value="{{ $params["search_query"] }}"
						@endif
						>
						<input type="text" name="search_by" 
						@if(isset($params["search_by"]))
						value="{{ $params["search_by"] }}"
						@endif 
						hidden>
						<div class="ui dropdown label">
							@if(isset($params["search_by"])) 
							<div class="text" data-search_by="{{ $params['search_by']}}">
								{{ TranslatorHelper::translate($params["search_by"]) }}
							</div>
							@else
							<div class="text" data-search_by="Name">
								{{TranslatorHelper::translate("Name")}}
							</div>
							@endif
							
							<i class="dropdown icon"></i>
							<div class="menu">
								<div class="item" data-type="Name">{{TranslatorHelper::translate("Name")}}</div>
								<div class="item" data-type="Description">{{TranslatorHelper::translate("Description")}}</div>
								<div class="item" data-type="Prices">{{TranslatorHelper::translate("Prices")}}</div>
							</div>
						</div>
						<button class="ui teal search button"><i class="search icon"></i>{{TranslatorHelper::translate("Search")}}</button>
					</div>
					<button class="ui teal fluid search button rwd"><i class="search icon"></i>{{TranslatorHelper::translate("Search")}}</button>
				</div>
			</div>



		</form>

		<a class="ui basic teal clear_filters button" href="{{ route("index.get") }}"><i class="delete icon"></i>{{TranslatorHelper::translate("Clear filters")}}</a>
	</div>


	@if(isset($items))
	<table class="ui fixed single line selectable small celled teal table">
		<thead>
			<th id="id">
				{{TranslatorHelper::translate("ID")}} 
				<span class="id sort" data-sort="id"><i class="sort icon"></i></span>
			</th>
			<th id="name">
				{{TranslatorHelper::translate("Name")}}
				<span class="name sort" data-sort="name"><i class="sort icon"></i></span>
			</th>
			<th id="description">
				{{TranslatorHelper::translate("Description")}}
				<span class="description sort" data-sort="description"><i class="sort icon"></i></span>
			</th>
			<th id="prices" width="200px">
				{{TranslatorHelper::translate("Prices")}}
				<span class="prices sort" data-sort="prices"><i class="sort icon"></i></span>
			</th>
			<th id="actions" width="80px">
				{{TranslatorHelper::translate("Actions")}}
			</th>
		</thead>
		<tbody>
			
			@foreach($items as $item)
			<tr data-id="{{ $item->id }}" data-edit_href="{{ route("product.edit.get", ["id" => $item->id]) }}">
				<td data-type="id">{{ $item->id }}</td>
				<td data-type="name">{{ $item->name }}</td>
				<td data-type="description">{{ $item->description }}</td>
				<td data-type="prices">
					<div class="ui list">
						@foreach($prices[$item->id] as $price)
						<div class="item">
							<i class="dollar icon"></i>
							<div class="content">
								{{ $price }}
							</div>
						</div>
						@endforeach
					</div>
					<div class="no_list">
						@foreach($prices[$item->id] as $price)
						<i class="dollar icon"></i>
						{{ $price }}
						@endforeach
					</div>
				</td>
				<td data-type="actions">
					<a class="circular ui teal edit button" href="{{ route("product.edit.get", ["id" => $item->id]) }}">
						<i class="cogs icon"></i>
					</a>
					<a class="circular ui teal edit fluid button rwd" href="{{ route("product.edit.get", ["id" => $item->id]) }}">
						<i class="cogs icon"></i>
					</a>
				</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
	
	<p>
		{{ $items_count }} / {{ $items_count_all }}
	</p>

	@include("templates/_pagination")

	@else

	<h2 class="ui center aligned icon header">
		<i class="ban icon"></i>
		{{TranslatorHelper::translate("No results")}}
	</h2>

	@endif

</div>

<div class="ui product_preview long modal">
	<div class="ui icon header">
		<i class="info circle icon"></i>
		{{TranslatorHelper::translate("Product info preview")}}
	</div>
	<div class="content">
		<div class="ui black segment">
			<h2 class="product_name"></h2>
			<div class="ui divider"></div>
			<p class="product_description"></p>
			<div class="ui teal segment">
				<div class="product_prices"></div>
			</div>
		</div>
	</div>
	<div class="actions">
		<div class="ui basic teal ok button">
			<i class="delete icon"></i>
			{{TranslatorHelper::translate("Cancel")}}
		</div>
		<a class="ui teal ok button" href="temp">
			<i class="cogs icon"></i>
			{{TranslatorHelper::translate("Edit")}}
		</a>
	</div>
</div>

@endsection

@section("styles")

@endsection

@section("scripts")

<script type="text/javascript" src="{{ asset("js/index.js") }}"></script>

@endsection