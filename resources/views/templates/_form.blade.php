<form id="productForm" class="ui form" method="POST" action="{{ $form_action }}">

	<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />

	<h2 class="ui header">
		<div class="content">
			{{ $form_title }}
		</div>
	</h2>
	<div class="ui teal segment">
		{{ $fields }}
	</div>
	<div class="ui error message">
		<div class="ui list">

		</div>
	</div>

	<button class="ui teal submit button"><i class="send icon"></i>{{TranslatorHelper::translate("Submit")}}</button>
	<a class="ui gray cancel button" href="{{ route("index.get") }}" ><i class="delete icon"></i>{{TranslatorHelper::translate("Cancel")}}</a>
	
</form>