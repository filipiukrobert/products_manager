
<div class="ui fixed secondary pointing menu">
	<div class="header item">
		<i class="list layout icon"></i>
		{{TranslatorHelper::translate("Products manager")}}
	</div>
	<a class="{{$home}} item" href="{{ route("index.get") }}">
		<i class="home icon"></i>{{TranslatorHelper::translate("Home")}}
	</a>
	<a class="{{$add_product}} item" href="{{ route("product.new.get") }}">
		<i class="plus icon"></i>{{TranslatorHelper::translate("Add product")}}
	</a>
	<div class="ui right dropdown item">
		<i class="globe icon"></i>{{TranslatorHelper::translate("Language")}}: {{ Config::get("app.locales")[App::getLocale()] }}
		<i class="dropdown icon"></i>
		<div class="menu">
			<a class="item" href="{{ route("locale.set", ["locale" => "pl"]) }}">PL</a>
			<a class="item" href="{{ route("locale.set", ["locale" => "en"]) }}">EN</a>
		</div>
	</div>

</div>

<div class="ui fixed secondary pointing rwd menu">

	<div class="header item">
		<i class="list layout icon"></i>
		{{TranslatorHelper::translate("Products manager")}}
	</div>

	<div class="ui dropdown item">
		<i class="list icon"></i>{{TranslatorHelper::translate("Menu")}}
		<i class="dropdown icon"></i>
		<div class="menu">
			<a class="item" href="{{ route("index.get") }}">
				<i class="home icon"></i>{{TranslatorHelper::translate("Home")}}
			</a>
			<a class="item" href="{{ route("product.new.get") }}">
				<i class="plus icon"></i>{{TranslatorHelper::translate("Add product")}}
			</a>
		</div>
	</div>

	<div class="ui dropdown item">
		<i class="globe icon"></i>{{TranslatorHelper::translate("Language")}}: {{ Config::get("app.locales")[App::getLocale()] }}
		<i class="dropdown icon"></i>
		<div class="menu">
			<a class="item" href="{{ route("locale.set", ["locale" => "pl"]) }}"><i class="pl flag"></i>PL</a>
			<a class="item" href="{{ route("locale.set", ["locale" => "en"]) }}"><i class="gb flag"></i>EN</a>
		</div>
	</div>

</div>