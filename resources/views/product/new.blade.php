@extends("master")

@section("menu")

@component("templates/_menu")
	@slot("home")
	@endslot
	@slot("add_product")
	active
	@endslot

@endcomponent

@endsection

@section("content")

<div class="ui black segment">

	@component("templates/_form")

	@slot("form_action")
	{{ route("product.new.post") }}
	@endslot

	@slot("form_title")
	<i class="big plus icon"></i>{{TranslatorHelper::translate("Add product")}}
	@endslot

	@slot("fields")

	<input class="all_prices" type="text" name="prices" hidden>

	<h4 class="ui dividing header">{{TranslatorHelper::translate("General infromations")}}</h4>
	<div class="field" data-name="name">
		<label>{{TranslatorHelper::translate("Name")}}</label>
		<input type="text" name="name" placeholder="{{TranslatorHelper::translate("Name")}}" >
	</div>
	<div class="field" data-name="description">
		<label>{{TranslatorHelper::translate("Description")}}</label>
		<textarea rows=3 name="description" placeholder="{{TranslatorHelper::translate("Description")}}" form="productForm" ></textarea>
	</div>
	<h4 class="ui dividing header">{{TranslatorHelper::translate("Prices")}} ( {{TranslatorHelper::translate("product can have multiple prices")}} )</h4>
	<div class="field" data-name="prices">
		<div class="ui action input">
			<input class="price" type="text" placeholder="0.00" pattern="(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)">
			<button class="ui teal button add_price" type="button"><i class="plus icon"></i></button>
		</div>
		<button class="ui teal fluid button add_price rwd" type="button"><i class="plus icon"></i></button>
	</div>
	<div class="field">
		<div class="prices" style="display: none;">
			<div class="ui relaxed divided animated list">

			</div>
		</div>

		<div class="no_items_message">
			{{TranslatorHelper::translate("This product doesn't have any price")}}.
		</div>

	</div>

	@endslot

	@endcomponent

	<div class="item reference" data-price="" style="display: none;">
		<i class="dollar icon"></i>
		<div class="content">
			
		</div>
	</div>

</div>

@endsection

@section("scripts")

<script type="text/javascript" src="{{ asset("js/FormManager.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/_form.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/productFormValidation.js") }}"></script>

@endsection