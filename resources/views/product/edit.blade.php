@extends("master")

@section("menu")

@component("templates/_menu")
	@slot("home")
	@endslot
	@slot("add_product")
	@endslot

@endcomponent

@endsection

@section("content")

<div class="ui black segment">

	@component("templates/_form")

	@slot("form_action")
	{{ route("product.edit.post", ["id" => $product->id]) }}
	@endslot

	@slot("form_title")
	<i class="big cogs icon"></i> {{TranslatorHelper::translate("Edit product")}}
	@endslot

	@slot("fields")

	<input type="number" name="product_id" value="{{ $product->id }}" hidden>
	<input class="all_prices" type="text" name="prices" hidden>

	<h4 class="ui dividing header">{{TranslatorHelper::translate("General infromations")}}</h4>
	<div class="field" data-name="name">
		<label>{{TranslatorHelper::translate("Name")}}</label>
		<input type="text" name="name" placeholder="{{TranslatorHelper::translate("Name")}}" value="{{ $product->name }}">
	</div>
	<div class="field" data-name="description">
		<label>{{TranslatorHelper::translate("Description")}}</label>
		<textarea rows=3 maxlength="255" name="description" placeholder="{{TranslatorHelper::translate("Description")}}" form="productForm">{{ $product->description }}</textarea>
	</div>
	<h4 class="ui dividing header">{{TranslatorHelper::translate("Prices")}} ( {{TranslatorHelper::translate("product can have multiple prices")}} )</h4>
	<div class="field" data-name="prices">
		<div class="ui action input">
			<input class="price" type="text" placeholder="0.00" pattern="(^([0-9]+)([/\\.])([0-9]{0,2})$)|(^[0-9]+$)">
			<button class="ui teal button add_price" type="button"><i class="plus icon"></i></button>
		</div>
		<button class="ui teal fluid button add_price rwd" type="button"><i class="plus icon"></i></button>
	</div>
	<div class="field">
		<div class="prices" style="display: none;">
			<div class="ui relaxed divided animated list">

				@foreach($prices[$product->id] as $price) 

				<div class="item" data-price="{{ $price }}">
					<i class="dollar icon"></i>
					<div class="content">
						{{ $price }}
						
					</div>
				</div>

				@endforeach

			</div>
		</div>

		<div class="no_items_message">
			{{TranslatorHelper::translate("This product doesn't have any price")}}.
		</div>

	</div>

	@endslot

	@endcomponent

	<div class="item reference" data-price="" style="display: none;">
		<i class="dollar icon"></i>
		<div class="content">
			
		</div>
	</div>

</div>

<div class="ui red delete segment">

	<h2 class="ui header">
		<div class="content">
			<i class="big delete icon"></i> {{TranslatorHelper::translate("Delete product")}}
		</div>
	</h2>

	<form id="deleteForm" class="ui form" method="post" action="{{ route("product.destroy.post", ["id" => $product->id]) }}">

		<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />

		<button id="delete_product" class="ui red button" type="button"><i class="trash outline icon"></i>{{TranslatorHelper::translate("Delete")}}</button>

	</form>

</div>

<div class="ui basic delete modal">
	<div class="ui icon header">
		<i class="warning sign icon"></i>
		{{TranslatorHelper::translate("Deleting product")}}
	</div>
	<div class="content">
		<h3>{{TranslatorHelper::translate("Warning")}}! {{TranslatorHelper::translate("Product will be deleted PERMANENTLY")}}!</h3>
	</div>
	<div class="actions">
		<div class="ui teal cancel inverted button">
			<i class="remove icon"></i>
			{{TranslatorHelper::translate("Cancel")}}
		</div>
		<div class="ui red ok button">
			<i class="trash icon"></i>
			{{TranslatorHelper::translate("Delete")}}
		</div>
	</div>
</div>

@endsection

@section("scripts")

<script type="text/javascript" src="{{ asset("js/FormManager.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/deleteProduct.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/_form.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/productFormValidation.js") }}"></script>

@endsection