<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config("app.name") }}</title>

	<link rel="stylesheet" type="text/css" href="{{ asset("css/semantic.min.css") }}">

	<link rel="stylesheet" type="text/css" href="{{ asset("css/general.css") }}">
	<link rel="stylesheet" type="text/css" href="{{ asset("css/rwd.css") }}">
	
	<script src="{{asset("js/jquery.min.js")}}"></script>
	<script src="{{ asset("js/semantic.min.js") }}"></script>
	
</head>
<body>

	@yield("menu")

	<div class="ui centered grid">
		<div class="ten wide column">
			@yield("content")
		</div>
	</div>

	<div id="loader_dimmer" class="ui active dimmer">
		<div class="ui loader"></div>
	</div>

	@yield("scripts")

	<script type="text/javascript">

		$('.ui.dropdown')
		.dropdown();

		$('.ui.checkbox')
		.checkbox();

		// $('.ui.modal')
		// .modal();

	</script>
</body>
</html>

