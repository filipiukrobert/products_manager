<?php

namespace App\Helpers;

class TranslatorHelper {

	public static function translate($json_key) {

		if( \App::isLocale("en") ) {
			return $json_key;
		} else {
			return __($json_key);
		}
	
	}

}