<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\ProductService;

class ProductController extends Controller
{

    public function getErrorsList() {

        $productService = new ProductService();
        return $productService->getErrorsList();
    }

    public function saveProduct(Request $request) {
    	
    	$parameters = $request->all();

        $errors = $this->validateParameters($parameters);

        if( empty($errors) ) {

            $productService = new ProductService();
            $productService->saveProduct($parameters);

            return redirect()->route("index.get");

        } else {

            return response(["status" => "failure", "errors" => $errors]);
        }
        
    }

    public function updateProduct(Request $request) {
    	$parameters = $request->all();

        $errors = $this->validateParameters($parameters);

        if( empty($errors) ) {

            $productService = new ProductService();
            $productService->updateProduct($parameters);

            return redirect()->route("index.get");

        } else {

            return response(["status" => "failure", "errors" => $errors]);
        }

    }

    public function destroyProduct($id) {

    	$productService = new ProductService();
    	$productService->destroyProduct($id);

    	return redirect()->route("index.get");
    }

    public function validateParameters($parameters) {

        $productService = new ProductService();
        return $productService->validateParameters($parameters);

    }

}
