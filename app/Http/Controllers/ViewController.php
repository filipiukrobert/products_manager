<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Config;
use App;
use Faker\Factory as Faker;

use App\Services\ProductService;
use App\Services\PaginationService;

use App\Models\Price;

class ViewController extends Controller
{
	public function index(Request $request) {

		$params = $request->all();

		$paramsString = $this->paramsToString($params);
		$formParams = [];
		parse_str($paramsString, $formParams);

		$items_count_all;
		$products;

		if(count($formParams) > 0) {

			$productService = new ProductService();
			$products = $productService->pluckProducts($formParams);
			if($products) {
				$items_count_all = count($products);
			}
			
		} else {

			$productService = new ProductService();
			$products = $productService->getProducts();
			$items_count_all = count($products);
		}

		if(count($products) > 0) {

			$page_number = 1;
			$products_per_page = 5;

			if(isset($params["per_page"]) && $params["per_page"] != "") {
				$products_per_page = $params["per_page"];
			}

			$paginationService = new PaginationService($request, $products, $products_per_page, 1);

			$pagination_array = $paginationService->getPaginationArray();
			$next_page = $paginationService->getNextPage();
			$prev_page = $paginationService->getPrevPage();
			$max_page = $paginationService->getMaxPage();
			$page_number = $paginationService->getPageNumber();

			$products_set = $products->forPage($page_number, $products_per_page);

			$products_ids = $products_set->map(function ($item) {
				return $item->id;
			})->toArray();
			$prices_raw = Price::whereIn("product_id", $products_ids)->select("product_id")->addSelect("value")->get();
			$prices_set = $this->preparePricesSet($prices_raw);

			$items_count = 0;

			for($i = 0; $i < $page_number; $i++) {
				$items_count += count($products->forPage($i+1, $products_per_page));
			}

			$sort_type = "";
			$sort_by = "";

			if(isset($formParams["sort_type"])){
				$sort_type = $formParams["sort_type"];
			}

			if(isset($formParams["sort_by"])){
				$sort_by = $formParams["sort_by"];
			}

			return view("index")
			->with("pagination_array", $pagination_array)
			->with("first_page", 1)
			->with("last_page", $max_page)
			->with("prev_page", $prev_page)
			->with("next_page", $next_page)
			->with("current_page", $page_number)
			->with("items", $products_set)
			->with("prices", $prices_set)
			->with("items_count", $items_count)
			->with("items_count_all", $items_count_all)
			->with("paramsString", $paramsString)
			->with("sort_type", $sort_type)
			->with("sort_by", $sort_by)
			->with("params", $params);
		} else {
			
			return view("index")
			->with("params", $params);
		}

	}

	private function formatPrice($price) {
		
		$value = (string)$price;

		if( strpos($value, ".") === false ) {
			$value .= ".00";
		} else {

			$valueSplit = explode(".",$value);
			$integer_part = $valueSplit[0];
			$fractional_part = $valueSplit[1];

			if( strlen($fractional_part) == 0 ) {
				$value .= "00";
			} else if ( strlen($fractional_part) == 1) {
				$value .= "0";
			}

		}

		return $value;
	}

	private function paramsToString($parameters) {

		$paramsString = "&";

		foreach ($parameters as $key => $value) {
			if($key != "page") {
				$paramsString .= "" . $key . "=" . $value . "&";
			}
		}

		return $paramsString;
	}

	public function newProduct() {

		return view("product.new");
	}

	public function editProduct($id) {

		$productService = new ProductService();
		$product = $productService->getProduct($id);

		$product_id = $id;
		$prices_raw = Price::where("product_id", $product_id)->select("product_id")->addSelect("value")->get();
		$prices_set = $this->preparePricesSet($prices_raw);

		return view("product.edit")
		->with("product", $product)
		->with("prices", $prices_set);
	}

	private function preparePricesSet($prices_raw) {

		$prices_set;

		foreach ($prices_raw as $price) {

			if( !isset($prices_set[$price->product_id]) ) {
				$prices_set[$price->product_id] = [];
			}

			array_push($prices_set[$price->product_id], $this->formatPrice($price->value));
		}

		return $prices_set;

	}

	public function setLocale($locale) {

		if( array_key_exists($locale, Config::get("app.locales") ) ) {

			App::setLocale($locale);
			Session::put('locale', $locale);
		}

		return redirect()->back();
	}

	public function getAvailableLocales() {
		
		return Config::get("app.locales");
	}
}
