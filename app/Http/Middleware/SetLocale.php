<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Config;
use App;

class SetLocale {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		if( Session::has('locale') ) {
			app()->setLocale(Session::get('locale'));
		} else {
			Session::put("locale", Config::get("app.fallback_locale"));
			app()->setLocale("en");
		}

		return $next($request);
	}
}



