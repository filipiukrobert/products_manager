<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Price;

use App\Services\ProductFormService;

class ProductService {

	protected $errors = [
		"empty" => "must be filled",
	];

	public function getErrorsList() {
		return $this->errors;
	}

	public function getProducts() {
		$products = Product::all();

		return $products;
	}

	public function getProduct($id) {
		$product = Product::find($id);

		return $product;
	}

	public function saveProduct($parameters) {

		$product = new Product;

		$product->name = $parameters["name"];
		$product->description = $parameters["description"];

		$product->save();

		$prices = json_decode($parameters["prices"]);
		$this->savePrices($product, $prices);

	}

	public function updateProduct($parameters) {

		$product = Product::find($parameters["product_id"]);

		$product->name = $parameters["name"];
		$product->description = $parameters["description"];

		$product->save();

		$deleted_prices = Price::where("product_id", $parameters["product_id"])->delete();

		$prices = json_decode($parameters["prices"]);
		$this->savePrices($product, $prices);
	}

	public function destroyProduct($id) {

		$prices = Price::where("product_id", $id)->delete();
		$product = Product::where("id", $id)->delete();

	}

	public function validateParameters($parameters) {

		$productFormService = new ProductFormService();
		$errors = $productFormService->validateParameters($parameters);

		return $errors;
	}

	private function savePrices($product, $prices_to_save) {

		foreach ($prices_to_save as $price_to_save) {
			$price = new Price;
			$price->value = (float)$price_to_save;
			$price->product_id = $product->id;

			$price->save();
		}
	}

	public function pluckProducts($parameters) {

		$params;
		$products_plucked;

		if( $parameters["search_query"] != "" ) {

			if( $parameters["search_by"] == "Prices" ) {

				$search_by_prices = true;

				$params = $this->prepareParametersArray($parameters, $search_by_prices);
				$products_plucked = Product::whereIn("id", $params);

			} else {

				$params = $this->prepareParametersArray($parameters);
				$products_plucked = Product::where($params);
			}
		} else {
			$products_plucked = Product::where("id",">",0);
		}

		if( isset($parameters["sort_by"]) && isset($parameters["sort_type"] ) && $parameters["sort_by"] != "" && $parameters["sort_type"] != "") {

			$sort_by = $parameters["sort_by"];
			$sort_type = $parameters["sort_type"];

			if( $sort_by == "prices" ) {

				$products_ids = $products_plucked->select("id")->get();
				$prices = [];

				$products_plucked = $this->pluckProductsByPricesSort($products_ids, $sort_type);

				return $products_plucked;

			}

			$products_plucked->orderBy($sort_by, $sort_type);

		}

		return $products_plucked->get();
	}

	private function pluckProductsByPricesSort($products_ids, $sort_type) {

		$ids = [];

		foreach ($products_ids as $product_id) {
			array_push($ids, $product_id->id);
		}
	

		$product_id_value_pairs = collect([]);
		$plucked_product_id_value_pairs = collect([]);

		$prices = Price::whereIn("product_id", $ids)->orderBy("product_id", "asc")->get();

		$product_id = $prices[0]->product_id;
		foreach ($prices as $price) {

			if( $product_id != $price->product_id || $prices->last() == $price)	{

				$max = $product_id_value_pairs->max($product_id);
				$plucked_product_id_value_pairs->push(["id"=> $product_id, "value" => $max]);
				$product_id_value_pairs = collect([]);

				$product_id = $price->product_id;
			}
			
			$product_id_value_pairs->push([$product_id => $price->value]);

		}

		$products_value_pairs_by_price_sort;

		if( $sort_type == "asc" ) {
			$products_value_pairs_by_price_sort = $plucked_product_id_value_pairs->sortBy("value");
		} else if( $sort_type == "desc" ) {
			$products_value_pairs_by_price_sort = $plucked_product_id_value_pairs->sortByDesc("value");
		}

		$finalIds = [];

		foreach ($products_value_pairs_by_price_sort as $product) {
			array_push($finalIds, $product["id"]);
		}
		$products = collect([]);

		foreach ($finalIds as $id) {
			$product = Product::where("id", $id)->get()[0];
			$products->push($product);
		}

		return $products;

	}

	private function prepareParametersArray($parameters, $search_by_prices = false) {

		$query_array = [];
		$search = [];

		if( !$search_by_prices ) {

			$search_by = strtolower($parameters["search_by"]);
			$search_query = $parameters["search_query"];

			$search = [$search_by, "like", "%". $search_query . "%"];

			$query_array = [
				$search,
			];

			$count = count($query_array);

			for($i = 0; $i < $count; $i++) {

				$temp_str = str_replace("%", "", $query_array[$i][2]);

				if($temp_str == "") {

					unset($query_array[$i]);
				}
			}

		} else {

			$search_value = (float)$parameters["search_query"];

			$price_query = [
				["value", "=", $search_value],
			];

			$prices = Price::where($price_query)->get();
			$products_ids = [];

			foreach ($prices as $price) {
				array_push($products_ids, $price["product_id"]);
			}

			$query_array = $products_ids;

		}

		return $query_array;
	}

}