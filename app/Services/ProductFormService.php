<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Price;

use App\Helpers\TranslatorHelper;

class ProductFormService {

	protected $isNameEmpty;
	protected $isDescriptionEmpty;
	protected $isPricesEmpty;

	protected $rules = [
		"empty" => "",
	];

	protected $errors = [];

	public function __construct() {
		
		$this->rules["empty"] = TranslatorHelper::translate("must be filled");


	}

	public function validateParameters($parameters) {
		
		$this->validateName($parameters["name"]);
		$this->validateDescription($parameters["description"]);
		$this->validatePrices($parameters["prices"]);

		return $this->errors;
	}

	public function validateName($parameter) {
		
		$this->isNameEmpty = $this->isEmpty($parameter);

		if($this->isNameEmpty) {
			$this->addError("name", "empty");
		}
	}

	public function validateDescription($parameter) {

		$this->isDescriptionEmpty = $this->isEmpty($parameter);

		if($this->isDescriptionEmpty) {
			$this->addError("description", "empty");
		}

	}

	public function validatePrices($parameter) {
		
		$this->isPricesEmpty = $this->isEmpty($parameter);

		if($this->isPricesEmpty) {
			$this->addError("prices", "empty");
		}
	}

	public function isEmpty($parameter) {
		if(empty($parameter)) {
			return true;
		} else {
			return false;
		}
	}

	private function addError($parameter, $rule) {
		
		switch ($parameter) {
			case 'name':
			$text = TranslatorHelper::translate("Field") . " " .
			TranslatorHelper::translate("Name") . 
			" : " . $this->rules[$rule];
			$this->createError($parameter, $text);
			break;

			case 'description':
			$text = TranslatorHelper::translate("Field") . " " .
			TranslatorHelper::translate("Description") . 
			" : " . $this->rules[$rule];
			$this->createError($parameter, $text);
			break;

			case 'prices':
			$text = TranslatorHelper::translate("Field") . " " .
			TranslatorHelper::translate("Prices") . 
			" : " . $this->rules[$rule];
			$this->createError($parameter, $text);
			break;
		}

	}

	private function createError($parameter, $text) {

		array_push($this->errors, ["parameter"=> $parameter, "text" => $text]);
	}

}