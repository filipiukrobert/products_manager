<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Price;

class Product extends Model
{
	protected $table = "products";

	public function price() {
		return $this->hasMany(Price::class);
	}
}
