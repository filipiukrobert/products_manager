<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Price extends Model
{
    protected $table = "prices";
    public $timestamps = false;

    protected $fillable = ["product_id","value"];

    public function product() {
    	return $this->belongsTo(Product::class);
    }
}
