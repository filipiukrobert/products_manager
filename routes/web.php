<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "index.get", "uses" => "ViewController@index"]);

Route::get("/{locale}", ["as" => "locale.set", "uses" => "ViewController@setLocale"]);

Route::get("/product/new", ["as" => "product.new.get", "uses" => "ViewController@newProduct"]);
Route::post("/product/new", ["as" => "product.new.post", "uses" => "ProductController@saveProduct"]);

Route::get("/product/{id}/edit", ["as" => "product.edit.get", "uses" => "ViewController@editProduct"]);
Route::post("/product/{id}/edit", ["as" => "product.edit.post", "uses" => "ProductController@updateProduct"]);

Route::post("/product/{id}/destroy", ["as" => "product.destroy.post", "uses" => "ProductController@destroyProduct"]);
